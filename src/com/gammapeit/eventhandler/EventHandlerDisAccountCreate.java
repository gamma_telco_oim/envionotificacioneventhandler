/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gammapeit.eventhandler;

import Thor.API.Operations.tcLookupOperationsIntf;
import Thor.API.tcResultSet;
import com.gammapeit.logger.helper.LoggerHelper;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import oracle.iam.configservice.api.Constants;
import oracle.iam.platform.Platform;
import oracle.iam.platform.context.ContextAware;
import oracle.iam.platform.context.ContextAwareNumber;
import oracle.iam.platform.kernel.spi.ConditionalEventHandler;
import oracle.iam.platform.kernel.spi.PostProcessHandler;
import oracle.iam.platform.kernel.spi.PreProcessHandler;
import oracle.iam.platform.kernel.vo.AbstractGenericOrchestration;
import oracle.iam.platform.kernel.vo.BulkEventResult;
import oracle.iam.platform.kernel.vo.BulkOrchestration;
import oracle.iam.platform.kernel.vo.EventResult;
import oracle.iam.platform.kernel.vo.Orchestration;
import org.apache.log4j.Logger;

/**
 *
 * @author Usuario
 */
public class EventHandlerDisAccountCreate implements PostProcessHandler, ConditionalEventHandler{
    
    
    //Logger logger = LoggerHelper.getLogger("CreateAccountDisconectedResourceNotification", EventHandlerDisAccountCreate.class);
    
    @Override
    public boolean isApplicable(AbstractGenericOrchestration ago) {
        System.out.println("[ProvisionAccountRNCEventHandler] isApplicable ");
        return true;
    }
    
    @Override
    public EventResult execute(long processID, long eventID, Orchestration o) {
       //logger.debug("Ingresa execute");
       System.out.println("************************Ingresa execute**********************************************");
       return executeEvent(o);
       //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BulkEventResult execute(long l, long l1, BulkOrchestration bo) {
        return new BulkEventResult();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean cancel(long l, long l1, AbstractGenericOrchestration ago) {
        return false;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void initialize(HashMap<String, String> hm) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void compensate(long l, long l1, AbstractGenericOrchestration ago) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public EventResult executeEvent(Orchestration param){
        try{
            System.out.println("************************************Ingresa execueEvent*********************************************");
            //logger.debug("Ingresa execueEvent");
            String msg="";
            //String operationType = param.getOperation();
              
            HashMap<String, Serializable> parameters = param.getParameters();
            
            Iterator parametros = parameters.entrySet().iterator();
            
            System.out.println("Parametos recibidos en el evento");
            //logger.debug("Parametos recibidos en el evento");
            while (parametros.hasNext()) {
            Map.Entry e = (Map.Entry)parametros.next();
            //logger.debug(e.getKey() + " " + e.getValue());
            System.out.println(e.getKey() + " " + e.getValue());
            
            }
            
            //logger.debug("Fin parametros");
            System.out.println("####################Fin parametros###################**");
            

            ContextAwareNumber canUSR = (ContextAwareNumber) parameters.get("usr_key");
            String usr_key = String.valueOf(((Long) canUSR.getObjectValue()));
            //logger.debug("USER_KEY: " + usr_key);
            System.out.println("USER_KEY: " + usr_key);

            //Obtener el usr_key del objeto orchestration : ej: 
            ContextAwareNumber canOBJ = (ContextAwareNumber) parameters.get("OBJ_KEY");
            String obj_key = String.valueOf(((Long) canOBJ.getObjectValue()));
            //logger.debug("OBK_KEY: " + obj_key);
            System.out.println("OBK_KEY: " + obj_key);
            
            HashMap<String,String> dataObject = ConsultaDatosOBJ(obj_key);//OBJ_NAME,OBJ_TYPE
            HashMap<String,String> dataUser = ConsultaDatosUsuario(usr_key);
            
            //if(DataObject.get("OBJ_TYPE").equalsIgnoreCase("disconnected")){}
            if(obj_key.equalsIgnoreCase("3405")){ //valida el object_key 3405 DEL RECURSO no conectado RNC0251 Prueba 
                                 
                String plataforma = dataObject.get("OBJ_NAME"); //Nombre de la plataforma
                
                //logger.debug("Cuenta provisionada en "+plataforma);
                System.out.println("Cuenta provisionada en "+plataforma);
                 
                 //logger.debug("Obtiene Administradores de plataformas (Lookup)");
                 System.out.println("Cuenta provisionada en "+plataforma);
                 HashMap<String,String> adminUsr = findLookups("Lookup.RNCWF.PlataformaDueno");// Obtiene Admin plataforma
                
                 if(!adminUsr.isEmpty()){
                //logger.debug("Lookup con Datos");
                 System.out.println("Lookup con Datos");
                 String admLogin = adminUsr.get(plataforma);
                 
                    if(!admLogin.isEmpty()){
                        System.out.println("Plataforma con Administrador");
                        //logger.debug("Plataforma con Administrador");

                        HashMap<String,String> dataAdmin = ConsultaDatosAdministrador(admLogin);

                        String correo = dataAdmin.get("USR_EMAIL");

                        if(!correo.isEmpty()){
                            System.out.println("Plataforma con Administrador");
                            //logger.debug("Plataforma con Administrador");
                            msg = "Se ha provisionado la plataforma "+plataforma+" al usuario: "+dataUser.get("USR_LOGIN")+" , "+dataUser.get("+\" \"+dataUser.get");
                            envioCorreos(correo,msg);
                        } else {
                            System.out.println("El administrador de la plataforma no tiene correo en OIM");
                            //logger.error("El administrador de la plataforma no tiene correo en OIM");
                        }

                    } else{
                        System.out.println("La plataforma no tiene administrador ");
                        //logger.error("La plataforma no tiene administrador ");
                    }
                 }else{
                     System.out.println("Lookup vacio");
                     //logger.error("Lookup vacio");
                 }
            }
            System.out.println("###############Se provisionó otro recurso#########################");
            //logger.debug("Se esta provisionando otro recurso");
        }catch(Exception ex){
            System.out.println("ex.getMessage()");
            //logger.error(ex.getMessage());
        }
        System.out.println("###################################################  TERMINA PROCESO  ######################################");
        //logger.debug("###################################################  TERMINA PROCESO  ######################################");
        return new EventResult();
    }
    
       public HashMap<String,String> findLookups(String pLookupName) throws Exception {
       System.out.println("Ingresa leer " + pLookupName);
       //logger.debug("Ingresa leer " + pLookupName);
       HashMap<String, String> data = new HashMap();
       try{
        tcLookupOperationsIntf lookupOperationsIntf = Platform.getService(tcLookupOperationsIntf.class);
        Map params = new HashMap();
        params.put("Lookup Definition.Code", pLookupName);

        tcResultSet tcR = lookupOperationsIntf.findLookupsDetails(params);

        for (int i = 0; i < tcR.getRowCount(); i++) {
            tcR.goToRow(i);
            String lookupCode = tcR.getStringValue("Lookup Definition.Code");
            String lookupType = tcR.getStringValue("Lookup Definition.Type");
            //Tipo L = Lookups creados para conectores
            if (lookupType.contains("l")) {
                tcResultSet tcRDecode = lookupOperationsIntf.getLookupValues(lookupCode);
                for (int j = 0; j < tcRDecode.getRowCount(); j++) {
                    tcRDecode.goToRow(j);
                    String lookupDetCode = tcRDecode.getStringValue(Constants.TableColumns.LKV_ENCODED.toString());
                    String lookupDetValue = tcRDecode.getStringValue(Constants.TableColumns.LKV_DECODED.toString());
                    data.put(lookupDetCode, lookupDetValue);
                    System.out.println(lookupDetCode + "\t" + lookupDetValue);
                    //logger.debug(lookupDetCode + "\t" + lookupDetValue);
                }
            }
        }
        System.out.println("\n\n****************************************Termina leer Lookup*****************************************************************************");
        //logger.debug("\n\n****************************************Termina leer Lookup*****************************************************************************");
       }catch(Exception ex){
           System.out.println(ex.getMessage());
           //logger.error(ex.getMessage());
       }
       return data;
    }
    
    private String getParamaterValue(HashMap<String, Serializable> parameters,String key) {
            if(parameters.containsKey(key)){
                    String value = (parameters.get(key) instanceof ContextAware) ? (String) ((ContextAware) parameters.get(key)).getObjectValue() : (String) parameters.get(key);
                    return value;
            }
            else{
                return null;
            }
    }
    
      public HashMap<String,String> ConsultaDatosUsuario(String usrkey) throws Exception{
          System.out.println("Ingresa consulta datos de usuario");
          //logger.debug("Ingresa consulta datos de usuario");
          Connection con =null;
          PreparedStatement pst =null;
          ResultSet rs = null;
          HashMap<String,String> data = new HashMap();
          try{
               con = Platform.getOperationalDS().getConnection();
               pst = con.prepareStatement("SELECT USR_LOGIN, USR_DISPLAY_NAME FROM USR  WHERE USR_KEY = ?");
               pst.setString(1, usrkey);
               rs = pst.executeQuery();
               while(rs.next()){
                   System.out.println("Obteniendo datos del usuario");
                   //logger.debug("Obteniendo datos del usuario");
                   data.put("USR_LOGIN", rs.getString("USR_LOGIN"));
                   data.put("USR_DISPLAY_NAME", rs.getString("USR_DISPLAY_NAME"));
               }

          }catch(Exception ex){
              System.out.println(ex.getMessage());
              //logger.debug(ex.getMessage());
          }finally{
              if(rs != null){
                  rs.close();
              }
              if(pst != null){
                  pst.close();
              }
              if(con != null){
                  con.close();
              }
          }
          System.out.println("********************************************Termina Consulta de datos******************************************************");
          //logger.debug("********************************************Termina Consulta de datos******************************************************");
          return data;
      }
      
      public HashMap<String,String> ConsultaDatosAdministrador(String usrlogin) throws Exception{
          System.out.println("Ingresa consulta datos de Administrador");
          //logger.debug("Ingresa consulta datos de Administrador");
          Connection con =null;
          PreparedStatement pst =null;
          ResultSet rs = null;
          HashMap<String,String> data = new HashMap();
          try{
               con = Platform.getOperationalDS().getConnection();
               pst = con.prepareStatement("SELECT USR_DISPLAY_NAME, USR_EMAIL FROM USR  WHERE USR_LOGIN = ?");
               pst.setString(1, usrlogin);
               rs = pst.executeQuery();
               while(rs.next()){
                   System.out.println("Obteniendo datos del Administrador de la plataforma");
                   //logger.debug("Obteniendo datos del Administrador de la plataforma");
                   data.put("USR_DISPLAY_NAME", rs.getString("USR_DISPLAY_NAME"));
                   data.put("USR_EMAIL", rs.getString("USR_EMAIL"));
               }

          }catch(Exception ex){
              System.out.println(ex.getMessage());
              //logger.debug(ex.getMessage());
          }finally{
              if(rs != null){
                  rs.close();
              }
              if(pst != null){
                  pst.close();
              }
              if(con != null){
                  con.close();
              }
          }
          System.out.println("***************************************Termina Consulta de datos de Administrador de Plataforma************************");
          //logger.debug("***************************************Termina Consulta de datos de Administrador de Plataforma************************");
          return data;
      }
      
      public HashMap<String,String> ConsultaDatosOBJ(String objkey) throws Exception{
          System.out.println("Ingresa consulta datos de Datos de Object");
          //logger.debug("Ingresa consulta datos de Datos de Object");
          Connection con =null;
          PreparedStatement pst =null;
          ResultSet rs = null;
          HashMap<String,String> data = new HashMap();
          try{
               con = Platform.getOperationalDS().getConnection();
               pst = con.prepareStatement("SELECT OBJ_NAME,OBJ_TYPE FROM OBJ WHERE OBJ_KEY =?");
               pst.setString(1, objkey);
               rs = pst.executeQuery();
               while(rs.next()){
                   System.out.println("Obteniendo datos del Administrador de la plataforma");
                   //logger.debug("Obteniendo datos del Administrador de la plataforma");
                   data.put("OBJ_NAME", rs.getString("OBJ_NAME"));
                   data.put("OBJ_TYPE", rs.getString("OBJ_TYPE"));
               }

          }catch(Exception ex){
              System.out.println(ex.getMessage());
              //logger.debug(ex.getMessage());
          }finally{
              if(rs != null){
                  rs.close();
              }
              if(pst != null){
                  pst.close();
              }
              if(con != null){
                  con.close();
              }
          }
          System.out.println("***************************************Termina Consulta de datos de Administrador de Plataforma************************");
          //logger.debug("***************************************Termina Consulta de datos de Administrador de Plataforma************************");
          return data;
      }
      
      private void envioCorreos(String to, String msg)//(String to, String msg)
      {
          System.out.println("Preparando correo");
          //logger.debug("Preparando correo");
            final String username = "gestionidentidades@telefonica.com";
            final String password = "";
            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "10.80.19.186");
            props.put("mail.smtp.port", "25");
            Session session = Session.getInstance(props,new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });
            try {
                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress("gestionidentidades@telefonica.com"));
                message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
                message.setSubject("Cuenta provisionada en plataforma de redes");
                message.setText(msg);
                message.setSentDate(new Date());
                Transport.send(message);
                System.out.println("Correo enviado a " + to);
                //logger.debug("Correo enviado a " + to);
            } catch (MessagingException e) {
                System.out.println(e.getMessage());
                //logger.error(e.getMessage());
                e.printStackTrace();
            }
      }



    
}
